const App = {
    data() {
        return {
            title: 'Notes',
            readOnly: true,
            input: {
                value: '',
                placeholder: 'Type...'
            },
            editItem: {
                id: '',
                value: ''
            },
            notes: []
        }
    },
    mounted() {
        this.getNotes()
    },
    watch: {
        notes: {
            handler(updatedList) {
                localStorage.setItem('notes', JSON.stringify(updatedList))
            },
            deep: true
        }
    },
    methods: {
        getNotes() {
            const localNotes = localStorage.getItem('notes')
            if (localNotes) {
                this.notes = JSON.parse(localNotes)
            }
        },
        onSubmit() {
            this.notes.push(this.input.value)
            console.log(this.input.value)
            this.input.value = ''
        },
        remove(index) {
            console.log(`note: ${index} has been removed`);
            this.notes.splice(index, 1);
        },
        onEdit(e) {
            this.readOnly = false
        },
        onSave(e) {
            this.notes[e.target.id] = e.target.value
            localStorage.setItem('notes', JSON.stringify(this.notes))
            this.getNotes()
            this.readOnly = true
        }
    }
}
Vue.createApp(App).mount('#app')
import { createRouter, createWebHistory, /* createWebHashHistory */ } from 'vue-router'
import HomePage from '@/pages/Home.vue';
import AboutPage from '@/pages/About.vue';
import NotFoundPage from '@/pages/NotFound.vue';

const routerHistory = createWebHistory();

const routers = createRouter({
    history: routerHistory,
    routes: [
        {
            name: 'home',
            path: '/',
            component: HomePage,
        },
        {
            name: 'about',
            path: '/about',
            component: AboutPage,
        },
        {
            name: 'NotFoundPage',
            // path: '/:PathMatch(.*)*',
            path: '/:CatchAll(.*)',
            component: NotFoundPage,
        },
    ]
})

export default routers;
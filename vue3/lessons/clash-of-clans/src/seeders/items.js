export default [
    {
        "id": 1,
        "title": "Archer",
        "descr": "descr01",
        "img": require("@/assets/img/468x60_archer.png"),
        "lvl": 4
    },
    {
        "id": 2,
        "title": "Wizard",
        "descr": "descr02",
        "lvl": 7,
        "img": require("@/assets/img/468x60_wizard.png")
    }
]
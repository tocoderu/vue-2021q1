import store from '@/store'

const setError = (error) => store.dispatch('setError', `Error: ${error}`)
const clearError = () => store.dispatch('setError', '')
const getError = () => store.getters.getError;

export { setError,  clearError,  getError};
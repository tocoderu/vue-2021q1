import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import HomePage from '@/pages/Home.vue'
import NotFoundPage from '@/pages/404.vue'
import AboutPage from '@/pages/About.vue'
import UsersPage from '@/pages/Users.vue'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage
        },
        {
            path: '/users',
            name: 'users',
            component: UsersPage
        },
        {
            path: '*',
            name: 'notFoundPage',
            component: NotFoundPage
        },

    ]
})
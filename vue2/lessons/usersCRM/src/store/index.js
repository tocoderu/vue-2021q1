import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)
import about from './about'
import users from './users'
export default new Vuex.Store({
    modules: {
        about, users,
    }
})


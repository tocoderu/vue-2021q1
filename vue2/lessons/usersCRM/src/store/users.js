export default {
    state: {
        users: 'hello vuex'
    },
    mutations: {
        setUsers(state, users) {
            state.users = users
        }
    },
    actions: {
        setUsers({commit}, payload) {
            commit('setUsers', payload )
        }
    },
    getters: {
        getUsers(state) {
            return state.users
        }
    },
}
import Vue from 'vue';

//UI
import Message from "@/components/UI/Message.vue";

// Controls
import AppButton from "@/components/UI/Controls/Button.vue";
import AppInput from "@/components/UI/Controls/Input.vue";
import AppTextArea from "@/components/UI/Controls/TextArea.vue";

//
import Intro from "~/components/ads/Intro.vue";
import PostsList from "~/components/Blog/PostList.vue";


Vue.component('AppButton', AppButton)
Vue.component('AppInput', AppInput)
Vue.component('AppTextArea', AppTextArea)
Vue.component('Message', Message)
Vue.component('Intro', Intro)
Vue.component('PostsList', PostsList)
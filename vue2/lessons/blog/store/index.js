import axios from 'axios';
import Cookie from 'js-cookie'

const baseURL = 'https://blog-nuxt-59e6b-default-rtdb.firebaseio.com/'
const signUpURL = "https://identitytoolkit.googleapis.com/v1/accounts:signUp?key="
const signInURL = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key='
const key = 'AIzaSyA3baBfyunvRr8paZ2aeeKJVhNCSaIEZd4'

export const state = () => ({
    postsLoaded: [],
    token: null
})
export const mutations = {
    addPost(state, post) {
        state.postsLoaded.push(post)
    },
    addPosts(state, posts) {
        state.postsLoaded = posts
    },
    editPost(state, postEdit) {
        const postIndex = state.postsLoaded.findIndex(post => post.id === postEdit.id)
        state.postsLoaded[postIndex] = postEdit
    },
    setToken(state, token) {
        state.token = token
    },
    destroyToken(state) {
        state.token = null
    }
}
export const actions = {
    authUser({
        commit
    }, authData) {
        const url = signInURL + key
        return axios.post(url, {
            email: authData.email,
            password: authData.password,
            returnSecureToken: true
        }).then(response => {
            const token = response.data.idToken
            commit('setToken', token)
            localStorage.setItem('token', token)
            Cookie.set('jwt', token)
        }).catch(e => console.log(e))
    },
    initAuth({
        commit
    }, request) {
        let token;
        if (request) {
            if (!request.headers.cookie) return false
            const jwtCookie = request.headers.cookie
                .split(';')
                .find(t => t.trim().startsWith('jwt='))
            if (!jwtCookie) { return false }
            token = jwtCookie.split('=')[1]
        } else {
            token = localStorage.getItem('token')
            if (!token) { return false }
        }

        commit('setToken', token)
    },
    logoutUser({
        commit
    }) {
        commit('destroyToken')
        localStorage.removeItem('token')
        Cookie.remove('jwt')
    },

    nuxtServerInit({
        commit
    }, context) {
        return axios
            .get(baseURL + 'posts.json')
            .then(response => {
                const postsArray = Object.entries(response.data).map(([name, post]) => ({
                    id: name,
                    ...post
                }));
                commit('addPosts', postsArray)
            })
    },
    addPost({
        commit
    }, post) {
        return axios.post(baseURL + 'posts.json', post)
            .then(response => {
                commit('addPost', {
                    id: response.data.name,
                    ...post
                })
            })
            .catch(error => console.log(error))
    },
    editPost({
        commit
    }, post) {
        return axios
            .put(baseURL + `posts/${post.id}.json?auth=${state.token}`, post)
            .then(response => {
                commit('editPost', post)
            })
            .catch(e => console.log(e))
    },
    addComment({
        commit
    }, comment) {
        return axios.post(baseURL + 'comments.json', comment).catch(e => console.log(e))
    }
}
export const getters = {
    getPostsLoaded(state) {
        return state.postsLoaded
    },
    checkAuthUser(state) {
        return state.token != null
    }
}

export default {
    state: {
        shopList: [
            {
                id: 1,
                title: 'Nike Red',
                description: 'Pariatur sint excepteur aute voluptate sunt in consequat nulla quis proident cupidatat ad.',
                img: require('@/assets/img/1.png'),
                gallery: [
                    {name: 'Nike first', img: require('@/assets/img/1.png')},
                    {name: 'Nike second', img: require('@/assets/img/2.png')},
                    {name: 'Nike thrid', img: require('@/assets/img/3.png')},
                ]
            },
            {
                id: 2,
                title: 'Nike Default',
                description: 'Pariatur sint excepteur aute voluptate sunt in consequat nulla quis proident cupidatat ad.',
                img: require('@/assets/img/4.png'),
                gallery: [
                    {name: 'Nike first', img: require('@/assets/img/4.png')},
                    {name: 'Nike second', img: require('@/assets/img/5.png')},
                    {name: 'Nike thrid', img: require('@/assets/img/6.png')},
                ]
            },
            {
                id: 3,
                title: 'Nike Street',
                description: 'Pariatur sint excepteur aute voluptate sunt in consequat nulla quis proident cupidatat ad.',
                img: require('@/assets/img/7.png'),
                gallery: [
                    {name: 'Nike first', img: require('@/assets/img/7.png')},
                    {name: 'Nike second', img: require('@/assets/img/8.png')},
                    {name: 'Nike thrid', img: require('@/assets/img/9.png')},
                ]
            },
            {
                id: 4,
                title: 'Nike Balance',
                description: 'Pariatur sint excepteur aute voluptate sunt in consequat nulla quis proident cupidatat ad.',
                img: require('@/assets/img/10.png'),
                gallery: [
                    {name: 'Nike first', img: require('@/assets/img/10.png')},
                    {name: 'Nike second', img: require('@/assets/img/11.png')},
                    {name: 'Nike thrid', img: require('@/assets/img/12.png')},
                ]
            },
        ]
    },
    mutations: {
        setMessage(state, message) {
            state.message = message
        }
    },
    actions: {
        setMessage({commit}, payload) {
            commit('setMessage', payload )
        }
    },
    getters: {
        getShopList(state) {
            return state.shopList
        },
        getProduct: (state) => (id) => {
            return state.shopList.find(
                product => product.id === id
            )
        }
    },
}
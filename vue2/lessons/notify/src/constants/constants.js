const URL_API = "http://jsonplaceholder.typicode.com/todos?_limit=";
const GET_NOTIFICATIONS_NUMBER = 5;
const GET_NOTIFY_TIMEOUT = 2000;
const SHOW_FIRST_NUMBER = 3;
const SET_MAIN_STEP = 2;

export {
  URL_API,
  GET_NOTIFICATIONS_NUMBER,
  GET_NOTIFY_TIMEOUT,
  SHOW_FIRST_NUMBER,
  SET_MAIN_STEP,
};

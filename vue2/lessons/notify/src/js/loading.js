import store from '@/store'
const showLoading = () => store.dispatch('setLoading', true)
const hideLoading = () => store.dispatch('setLoading', false)
const getLoading = () => store.getters.getLoading

export { showLoading, hideLoading, getLoading };
import axios from "axios";
import { setError, clearError } from "@/js/error";
import { showLoading, hideLoading } from "@/js/loading";
import { setMessage } from "@/js/message";
import {
  URL_API,
  GET_NOTIFICATIONS_NUMBER,
  GET_NOTIFY_TIMEOUT,
  SHOW_FIRST_NUMBER,
} from "@/constants/constants.js";

const getNotify = async (limit = GET_NOTIFICATIONS_NUMBER) => {
  axios
    .get(URL_API + limit)
    .then((response) => {
      const result = response.data.map(convertItem);
      result
        .filter((item, index) => index < SHOW_FIRST_NUMBER)
        .forEach(item => item.main = true);
      setMessage(result);
    })
    .catch((error) => {
      console.log(error.message);
      setError(error.message);
    })
    .finally(() => {
      hideLoading();
    });
};
const getNotifyLazy = () => {
  clearError();
  showLoading();
  setTimeout(() => {
    getNotify(10);
  }, GET_NOTIFY_TIMEOUT);
};

const convertItem = (item) => ({ id: item.id, title: item.title, main: false });

export { getNotifyLazy, getNotify };

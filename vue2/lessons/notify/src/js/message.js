import store from "@/store";

const setMessage = (message) => store.dispatch("setMessage", message);
const loadMessages = () => store.dispatch("loadMessages");
const getMainMessages = () => store.getters.getMessageMain;
const isEmptyFilteredMessages = () => store.getters.getMessageFilter.length === 0 ? true : false;

export { setMessage, loadMessages, getMainMessages, isEmptyFilteredMessages };

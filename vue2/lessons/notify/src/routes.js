import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router);

import HomePage from '@/pages/HomePage.vue'
import NotFoundPage from '@/pages/404Page.vue'
import NotifyPage from '@/pages/NotifyPage.vue'
import AboutPage from '@/pages/AboutPage.vue'

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'home',
            component: HomePage
        },
        {
            path: '/notify',
            name: 'notify',
            component: NotifyPage
        },
        {
            path: '/about',
            name: 'about',
            component: AboutPage
        },
        {
            path: '*',
            name: 'notFoundPage',
            component: NotFoundPage
        },

    ]
})